package controllers;

import models.*;
import play.mvc.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: Thang Nguyen
 * Date: 1/9/13
 * Time: 4:13 PM
 */
public class Home extends Controller {
    public static void view() {
        List<Client> allClients= Client.findAll();
        List<TaskDone> allTasksDone = TaskDone.findAll();

        List<Task> allTasks = Task.findAll();
        render(allClients,allTasksDone, allTasks);
    }

    public static void changeStatus(long taskId, long clientId) {
        Client client = Client.findById(clientId);
        Task task = Task.findById(taskId);
        TaskDone taskDone = new TaskDone(client,task,true).save();
        redirect("/Home/view");
    }
    
    public static void viewNotes() {
        
    }
}
