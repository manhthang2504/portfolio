/*
 * @Author: Nguyen Manh Thang
 * @Date: Jan 16, 2013
 */
package controllers;

import models.*;
import play.mvc.*;

import java.util.Date;

/**
 *
 * @author thangnguyen
 */
public class FollowUp extends Controller {
    public static void view() {        
        render();
    }

    public static void add(long clientId, String staff, String section, Date date, boolean isReceived) {
        Client client = Client.findById(clientId);
        FileStatusFollowUp followUp = new FileStatusFollowUp(client, staff, section, date, isReceived).save();
    }
}
