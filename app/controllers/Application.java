package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Application extends Controller {

    public static void index(long id) {
        Client client = Client.findById(id);
        List<TaskDone> taskManagement = TaskDone.find("byClient",client).fetch();
        render(taskManagement);
    }

}