/*
 * @Author: Nguyen Manh Thang
 * @Date: Jan 16, 2013
 */
package models;

import java.util.*;
import javax.persistence.*;
import play.db.jpa.*;

/**
 *
 * @author thangnguyen
 */
@Entity
public class FileStatusFollowUp extends Model {
    public Client client;
    public String staff;
    public String section;
    public Date date;
    public boolean isReceived;
    
    public FileStatusFollowUp (Client client, String staff, String section, Date date, boolean isReceived) {
        this.client = client;
        this.staff = staff;
        this.section = section;
        this.date = date;
        this.isReceived = isReceived;
    }
    
}
