package models;

import play.db.jpa.Model;

import javax.persistence.*;


/**
 * Created with IntelliJ IDEA.
 * Author: Thang Nguyen
 * Date: 1/8/13
 * Time: 9:42 AM
 */
@Entity
public class Task extends Model {
    public String name;

    public Task (String name) {
        this.name = name;
    }
}
