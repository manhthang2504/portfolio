package models;
import play.db.jpa.*;
import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * Author: Thang Nguyen
 * Date: 1/17/13
 * Time: 4:09 PM
 */
@Entity
public class Account extends Model {
    public int accountNumber;
    public String accountName;
    public String note;

    public Account (int accountNumber, String accountName, String note) {
        this.accountNumber = accountNumber;
        this.accountName = accountName;
        this.note = note;
    }
}
