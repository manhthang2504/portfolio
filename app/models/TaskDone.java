package models;

import play.db.jpa.*;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * Author: Thang Nguyen
 * Date: 1/8/13
 * Time: 2:40 PM
 */
@Entity
public class TaskDone extends Model {
    @ManyToOne
    public Client client;

    @ManyToOne
    public Task task;

    public boolean isDone;

    public TaskDone (Client client, Task task, boolean isDone) {
        this.client = client;
        this.task = task;
        this.isDone = isDone;
    }
}
