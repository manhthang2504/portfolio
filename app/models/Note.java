package models;

import play.db.jpa.*;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * Author: Thang Nguyen
 * Date: 1/15/13
 * Time: 3:41 PM
 */
@Entity
public class Note extends Model {
    @ManyToOne
    public Task task;
    public String note;

    public Note (Task task, String note) {
        this.task = task;
        this.note = note;
    }
}
