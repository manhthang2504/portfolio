package models;

import play.db.jpa.*;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * Author: Thang Nguyen
 * Date: 1/8/13
 * Time: 9:13 AM
 */
@Entity
public class Client extends Model {
    public String name;

    public Client (String name) {
        this.name = name;
    }
}
